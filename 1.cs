#!/usr/bin/csharp
// sudo apt-get -y install mono-csharp-shell

var sum = Enumerable.Range(0, 1000).Where(x => x % 5 == 0 || x % 3 == 0).Sum();
System.Console.WriteLine(sum);
