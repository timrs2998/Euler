#!/usr/bin/env groovy
final def N = 4000000

def prev1 = 1
def prev2 = 2
def sum = 2
while (true) {
    def f = prev1 + prev2
    if (f > N) {
        break
    }
    if (f % 2 == 0) {
        sum += f
    }

    prev1 = prev2
    prev2 = f
}
println sum
