#!/usr/bin/env node
'use strict'

const n = 1000
const sum = Array.from(new Array(n), (x, i) => i)
    .filter(x => x % 5 === 0 || x % 3 === 0)
    .reduce((a, b) => a + b)
console.log(sum)
