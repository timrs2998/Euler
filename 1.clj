#!/usr/bin/env boot
(def answer (reduce + (filter (fn [x] (or (= (mod x 5) 0)
                                          (= (mod x 3) 0)))
                              (range 1000))))
(println answer)

