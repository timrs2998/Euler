#!/usr/bin/env bash
/usr/bin/g++ -std=c++14 -x c++ -o 1.out - <<EVILEOF
#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>

int main() {
    using std::cout;
    using std::endl;
    using std::vector;
    using std::iota;
    using std::accumulate;

    const auto n = 1000;

    // Classical approach
//    auto sum = 0;
//    for (auto i = 0; i < n; i++) {
//        if (i % 5 == 0 || i % 3 == 0) {
//            sum += i;
//        }
//    }
//    cout << "Answer: " << sum << endl;

    // "Other" approach (not easy in C++)

    // Get list of numbers 0 to n
    vector<int> numbers(n);
    iota(numbers.begin(), numbers.end(), 0);

    // Filter list to numbers divisible by 3 or 5
    vector<int> divisible(n);
    auto it = std::copy_if(
        numbers.begin(),
        numbers.end(),
        divisible.begin(),
        [](int i){return i % 5 == 0 || i % 3 == 0;}
    );
    divisible.resize(std::distance(divisible.begin(), it));

    // Sum the new list
    int sum = std::accumulate(
        divisible.begin(),
        divisible.end(),
        0
    );
    cout << "Answer: " << sum << endl;
}
EVILEOF
./1.out
rm 1.out
