#!/usr/bin/env groovy

long sumOfSquares(int n) {
    (1..n).collect { it * it }.sum()
}

long squareOfSum(int n) {
    Math.pow((1..n).sum(), 2)
}

println squareOfSum(100) - sumOfSquares(100)
