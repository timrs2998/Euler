#!/usr/bin/env perl
use strict;
use v5.16;
use List::Util qw(sum);

my $answer = sum grep($_ % 3 == 0 || $_ % 5 == 0, 0..999);
print $answer;
