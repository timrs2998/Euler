#!/usr/bin/env bash
/usr/bin/gfortran -std=f2008 -x f95 -o 1.out - <<EVILEOF
program one
    integer :: i
    integer :: sum = 0

    do i = 0,999
        if (mod(i, 3) == 0 .or. mod(i, 5) == 0) then
            sum = sum + i
        endif
    enddo

    write(*, *) 'Answer: ', sum
end program
EVILEOF
./1.out
rm 1.out
