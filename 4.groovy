#!/usr/bin/env groovy

boolean isPalindrome(long n) {
    n.toString() == n.toString().reverse()
}

// Naive approach
def findPalindromes() {
    def candidates = []
    for (int i = 999; i >= 0; i--) {
        for (int j = 999; j >= 0; j--) {
            def product = i * j
            if (isPalindrome(product)) {
                candidates << product
            }
        }
    }
    candidates
}

def values = findPalindromes()
println values.max()

