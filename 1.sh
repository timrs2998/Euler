#!/bin/bash
set -e

n=1000
sum=0
for ((i = 1; i <= n - 1; i++)); do
    if ((i % 5 == 0 || i % 3 == 0)); then
        sum=$((sum += i))
    fi
done
echo $sum
