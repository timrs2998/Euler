#!/usr/bin/env bash
/usr/bin/gcc -std=c11 -xc -o 1.out - <<EVILEOF
#include <stdio.h>
int main() {
    int sum = 0;

    for (int i = 0; i < 1000; i++) {
        if (i % 3 == 0 || i % 5 == 0) {
            sum += i;
        }
    }

    printf("Answer: %d\n", sum);

    return 0;
}
EVILEOF
./1.out
rm 1.out
