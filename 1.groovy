#!/usr/bin/env groovy
def answer = (0..999).findAll {
    it % 3 == 0 || it % 5 == 0
}.sum()

println answer
